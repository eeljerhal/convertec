<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImgProducto extends Model
{
    protected $table = "imgproductos";


    protected $fillable = [
        'producto_id', 'path', 'thumbnail',
    ];

    public function producto()
    {
    	return $this->belongsTo('Producto');
    }

    
}