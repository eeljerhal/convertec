<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use \App\Tipo;

class TiposController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        Tipo::create([
        	'tipo' => $request['tipo'],
        ]);

        //return redirect()->route('tipos.create')->with('message', 'store');
        return back()->with('message', 'tipo_add');
    }

    public function update(Request $request)
    {
        $tipo = Tipo::findOrFail($request->tipo_id);
        $tipo->update($request->all());
        return back()->with('message', 'tipo_edit');
    }

    public function destroy(Request $request)
    {
        $tipo = Tipo::findOrFail($request->tipo_id);
        $tipo->delete();
        return back()->with('message', 'tipo_del');
    }

    //Datatables
    public function get_data(){
        $tipo = Tipo::select('id', 'tipo');
        return DataTables($tipo)->make(true);
    }

}
