<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \App\ImgProducto;
use \App\Producto;

class ImgProductoController extends Controller
{

	public function index ($id){
		$producto = Producto::findOrFail($id);
		//$cod = Producto::select('codigo')->where('id', '=', $id);
		return View('admin.addimgproducto')->with('prod', $producto);

	}

	public function upload(Request $request){
        $image = $request->file('file');
        //$id_img = $request->id_img;
        $imageName = $request->id_prod.("-").$image->getClientOriginalName();
        $path = "img/productos";
        $upload_success = $image->move($path,$imageName);
        $path = $path.("/").$imageName;
        /*if ($upload_success) {
        	return response()->json($upload_success, 200);
        }else{
        	return response()->json('error', 400);
        }*/
        if ($upload_success) {
        	/*ImgProducto::create([
	            'producto_id' => $request['id_prod'],
		        'path' => $path,
		        'thumbnail' => 1,
		    ]);*/
		    return response()->json($upload_success, 200);	//CONFIGURAR MAX UPLOAD SIZE DE PHP.INI
        	/*if ($id_img == "1") {
        		ImgProducto::create([
		            'producto_id' => $request['id_prod'],
			        'path' => $id_img,
			        'thumbnail' => 1,
		        ]);
        	}else{
        		ImgProducto::create([
		            'producto_id' => $request['id_prod'],
			        'path' => $id_img,
			        'thumbnail' => 0,
		        ]);
        	}*/
	        //return response()->json($upload_success, 200);	//CONFIGURAR MAX UPLOAD SIZE DE PHP.INI
	    }
	    // Else, return error 400
	    else {
	        return response()->json('error', 400);
	    }
    }

    public function delete() {
		/*$image = 'img/productos'.$img;
		Storage::delete($image);
		return 'Yup it worked!';*/
			$url = "img/productos/";
			$url .= $_GET['nombre'];
			if (file_exists($url)) {
		        unlink($url);
		    }
	}
}
