<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use \App\Categoria;

class CategoriasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        
        Categoria::create([
        	'categoria' => $request['categoria'],
        ]);

        return back()->with('message', 'categoria_add');
    }

    public function update(Request $request)
    {
        $categoria = Categoria::findOrFail($request->categoria_id);
        $categoria->update($request->all());
        return back()->with('message', 'categoria_edit');
    }

    public function destroy(Request $request)
    {
        $categoria = Categoria::findOrFail($request->categoria_id);
        $categoria->delete();
        return back()->with('message', 'categoria_del');
    }

    //Datatables
    public function get_data(){
        $categoria = Categoria::select('id', 'categoria');
        return DataTables($categoria)->make(true);
        
    }

}
