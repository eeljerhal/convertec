<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use \App\Producto;
use \App\Categoria;
use \App\Marca;
use \App\Tipo;
use \App\ImgProducto;

class ProductosController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        return View('admin.addproductos')
        ->with('categorias', Categoria::All())
        ->with('marcas', Marca::All())
        ->with('tipos', Tipo::All());
    }

    public function store(Request $request)
    {
        $codigo = $request->get('codigo');
        $image = $request->file('imagen'); //CONFIGURAR MAX UPLOAD SIZE DE PHP.INI
        $imageName = $image->getClientOriginalName();
        $path = "img/productos";
        $upload_success = $image->move($path,$imageName);
        $path = $path.("/").$imageName;
        if ($upload_success) {
            $producto = Producto::create([
                'codigo' => $request['codigo'],
                'tipo_id' => $request['tipo'],
                'marca_id' => $request['marca'],
                'categoria_id' => $request['categoria'],
                'descripcion' => $request['descripcion'],
            ]);

            ImgProducto::create([
                'producto_id' => $producto->id,
                'path' => $path,
                'thumbnail' => 1,   //Se busca implementar una forma para agregar mas de una imagen y que una imagen tenga el atributo de thumbnail para mostrar en la lista.
            ]);
            return back()->with('message', 'producto_add');
        }
        // Else, return error 400
        else {
            return response()->json('error', 400);
        }

        //=====================================================//
        /*
        //$categoria = $request->get('categoria');
        //$tipo = $request->get('tipo');
        //$marca = $request->get('marca');
        $codigo = $request->get('codigo');
        //$descripcion = $request->get('descripcion');

        /*$dir = public_path().'/img/productos';
        $images = $request->file('file');

        foreach ($images as $image) {
            $imageName = $image->$codigo.("-").$image->getClientOriginalName();
            $image->move($dir, $imageName);
        }
        
        
        /*return response()->json(array(
            'categoria'=> $categoria,
            'tipo'=> $tipo,
            'marca'=> $marca,
            'codigo'=> $codigo,
            'descripcion'=> $descripcion
        ));

        $producto = Producto::create([
            'codigo' => $request['codigo'],
            'tipo_id' => $request['tipo'],
            'marca_id' => $request['marca'],
            'categoria_id' => $request['categoria'],
            'descripcion' => $request['descripcion'],
        ]);
        
        /*return View('admin.addimgproducto')
        ->with('cod', $codigo);
        $id = ($producto->id);
        return redirect()->route('upload_img', ['id' => $id]);     */   

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        /*$image =  ImgProducto::where('producto_id', $id)->firstOrFail();
        $path =     DISPLAY DE LA IMAGEN AL EDITAR
        $adjusted = str_start('/this/string/', '');*/
        $image = "../../../".ImgProducto::where('producto_id', $id)->firstOrFail()->path;
        $imageName = str_after($image, 'img/productos/');

        return View('admin.editproductos')
        ->with('categorias', Categoria::All())
        ->with('marcas', Marca::All())
        ->with('tipos', Tipo::All())
        ->with('producto', Producto::findOrFail($id))
        ->with('image', $image)
        ->with('imageName', $imageName);
    }

    public function update(Request $request)
    {
        $imageName = $request->file('imagen')->getClientOriginalName();
        $path = "img/productos/".$imageName;

        $producto = Producto::findOrFail($request->producto_id);
        $imgProducto = ImgProducto::where('producto_id', $request->producto_id)->firstOrFail();
        
        $producto->update($request->all());
        $imgProducto->update(['path' => $path]);
        
        return redirect()->route('admin')->with('message', 'producto_edit');
    }

    public function destroy(Request $request)
    {
        $producto = Producto::findOrFail($request->producto_id);
        $producto->delete();
        return back()->with('message', 'producto_del');
    }

    //datatable
    public function get_data(){
        $producto = Producto::join('marcas', 'marca_id', '=', 'marcas.id')
            ->join('tipos', 'tipo_id', '=', 'tipos.id')
            ->join('categorias', 'categoria_id', '=', 'categorias.id')
            ->join('imgproductos', 'producto_id', '=', 'productos.id')
            ->select('productos.id', 'productos.codigo', 'productos.descripcion', 'tipos.tipo', 'categorias.categoria', 'marcas.marca', 'imgproductos.path')
            ->where('imgproductos.thumbnail', 1)
        ->get();
        
        return DataTables($producto)->make(true);
    }

}
