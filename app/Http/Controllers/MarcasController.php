<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use \App\Marca;

class MarcasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        Marca::create([
        	'marca' => $request['marca'],
        ]);

        //return redirect()->route('marcas.create')->with('message', 'store');
        return back()->with('message', 'marca_add');
    }

    public function update(Request $request)
    {
        $marca = Marca::findOrFail($request->marca_id);
        $marca->update($request->all());
        return back()->with('message', 'marca_edit');
    }

    public function destroy(Request $request)
    {
        $marca = Marca::findOrFail($request->marca_id);
        $marca->delete();
        return back()->with('message', 'marca_del');
    }

    //Datatables
    public function get_data(){
        $marca = Marca::select('id', 'marca');
        return DataTables($marca)->make(true);
    }
}
