<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin/instructivo', function () {
    return view('admin.instructivo');
});

Auth::routes();

//Route::get('/admin', 'PrincipalController@index');
Route::get('/admin', ['as' => 'admin', 'uses' => 'PrincipalController@index']);
Route::get('/admin/productos/get_data_productos', ['as' => 'get_data_productos', 'uses' => 'ProductosController@get_data']);
Route::get('/admin/categorias/get_data_categorias', ['as' => 'get_data_categorias', 'uses' => 'CategoriasController@get_data']);
Route::get('/admin/tipos/get_data_tipos', ['as' => 'get_data_tipos', 'uses' =>'TiposController@get_data']);
Route::get('/admin/marcas/get_data_marcas', ['as' => 'get_data_marcas', 'uses' =>'MarcasController@get_data']);

//Route::get('/admin/productos/{id}/upload', ['as' => 'upload_img', 'uses' =>'ImgProductoController@index']);
//Route::get('/admin/img_remove', ['as' => 'remove_img', 'uses' =>'ImgProductoController@delete']);
//Route::post('/admin/img_upload', ['as' => 'upload', 'uses' =>'ImgProductoController@upload']);
Route::resource('/admin/productos', 'ProductosController');
Route::resource('/admin/categorias', 'CategoriasController');
Route::resource('/admin/marcas', 'MarcasController');
Route::resource('/admin/tipos', 'TiposController');
