$(document).ready(function(){
    productos();
    categorias();
    tipos();
    marcas();
});

var lang = {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        };

//Tabla productos
var productos = function(){
    var prod_table = $('#productos-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: true,
        language: lang,
        scrollY: "500px",
        scrollX: false,
        
        ajax: {
            url : route.productos, //variable que guarda la ruta --- referencia en admin.blade
            datatype: "json"
        },
        columns: [
            { data : 'path', name: 'path',
                render : function ( data, type, row, meta ) {
                    return '<img class="img-table" src="'+ data +'">';
                } 
            },
            { data: 'codigo', name: 'codigo'},
            { data: 'categoria', name: 'categoria'},
            { data: 'tipo', name: 'tipo'},
            { data: 'descripcion', name: 'descripcion'},
            { data: 'marca', name: 'marca'},
            { data: 'id', name: 'id',
                render : function (data) {
                    return renderHtmlProd('producto', data);
                    /*return '<div class= "row">\
                                <div class="center-block">\
                                    <button type="button" id="eliminar" class="eliminar btn btn-danger btn-tabla center-block" data-toggle="modal" data-target=#delete-producto-modal>\
                                        <i class="fa fa-trash"></i>\
                                    </button>\
                                </div>\
                                <div class="center-block">\
                                    <a href="/admin/productos/'+ data +'/edit" class="editar btn btn-success btn-tabla center-block" style="margin-bottom: 0px;">\
                                        <i class="fa fa-pencil"></i>\
                                    </a>\
                                </div>\
                            </div>'*/
            }}
        ],
        columnDefs: [
            { targets: 4,
                mRender: function ( data, type, full ) {    //renderear el html
                            return $("<div/>").html(data).text(); 
                        },
                width: "35%"
            },
            { targets: 0, orderable: false, searchable: false, width: "10%"},
            { targets: 6, orderable: false, searchable: false, width: "9%"}
        ],
        order: [
            [ 1, "asc" ]
        ]
    });

    obtener_data_productos("#productos-table tbody", prod_table);
}



//Tabla categorias
var categorias = function(){
    var number = 0;
    var cat_table = $('#categorias-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: true,
        language: lang,
        ajax: {
            url: route.categorias,
            datatype: "json"
        },
        columns: [
            { data: 'categoria', name: 'categoria'},
            { data: 'id', name: 'id',
                render : function (data) {
                    return renderHtml('categoria', data);
            }}
        ],
        columnDefs: [
            { targets: 1, orderable: false, searchable: false, width: "22%"}
        ]
    });

    obtener_data_categorias("#categorias-table tbody", cat_table);
}


//tabla tipos
var tipos = function(){
    var tipos_table = $('#tipos-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: true,
        language: lang,
        ajax: {
            url: route.tipos,
            datatype: "json"
        },
        columns: [
            { data: 'tipo', name: 'tipo'},
            { data: 'id', name: 'id',
                render : function (data) {
                    return renderHtml('tipo', data);
            }}
        ],
        columnDefs: [
            { targets: 1, orderable: false, searchable: false, width: "22%"}
        ]
    });
    
    obtener_data_tipos("#tipos-table tbody", tipos_table);
}

//tabla marcas
var marcas = function(){
    var marcas_table = $('#marcas-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: true,
        language: lang,
        ajax: {
            url: route.marcas,
            datatype: "json"
        },
        columns: [
            { data: 'marca', name: 'marca'},
            { data: 'id', name: 'id',
                render : function (data) {
                    return renderHtml('marca', data);
            }}
        ],
        columnDefs: [
            { targets: 1, orderable: false, searchable: false, width: "22%"}
        ]
    });
    obtener_data_marcas("#marcas-table tbody", marcas_table);
}

var obtener_data_productos = function(tbody, table){
    $(tbody).on("click", "a.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        console.log("click", data.id);
        /*$('#edit-producto-modal').on("show.bs.modal", function( event ){
            var modal = $(this);
            modal.find('.modal-body #txt_prod').val(data.categoria);
            modal.find('.modal-body #prod_id').val(data.id);
        });*/
    });

    $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        $('#delete-producto-modal').on("show.bs.modal", function( event ){
            var modal = $(this);
            document.getElementById('producto').innerHTML = 'Estas seguro que quieres eliminar el producto "' + data.codigo + '"?';
            modal.find('.modal-body #prod_id').val(data.id);
        });
    });
}

var obtener_data_categorias = function(tbody, table){
    $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        $('#edit-categoria-modal').on("show.bs.modal", function( event ){
            var modal = $(this);
            modal.find('.modal-body #txt_cat').val(data.categoria);
            modal.find('.modal-body #cat_id').val(data.id);
        });
    });

    $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        $('#delete-categoria-modal').on("show.bs.modal", function( event ){
            var modal = $(this);
            document.getElementById('categoria').innerHTML = 'Estas seguro que quieres eliminar la categoria "' + data.categoria + '"?';
            modal.find('.modal-body #cat_id').val(data.id);
        });
    });
}
var obtener_data_tipos = function(tbody, table){
    $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        $('#edit-tipo-modal').on("show.bs.modal", function( event ){
            var modal = $(this);
            modal.find('.modal-body #txt_tipo').val(data.tipo);
            modal.find('.modal-body #tipo_id').val(data.id);
        });
    });

    $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        $('#delete-tipo-modal').on("show.bs.modal", function( event ){
            var modal = $(this);
            document.getElementById('tipo').innerHTML = 'Estas seguro que quieres eliminar el tipo "' + data.tipo + '"?';
            modal.find('.modal-body #tipo_id').val(data.id);
        });
    });
}
var obtener_data_marcas = function(tbody, table){
    $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        $('#edit-marca-modal').on("show.bs.modal", function( event ){
            var modal = $(this);
            modal.find('.modal-body #txt_marca').val(data.marca);
            modal.find('.modal-body #marca_id').val(data.id);
        });
    });

    $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        $('#delete-marca-modal').on("show.bs.modal", function( event ){
            var modal = $(this);
            document.getElementById('marca').innerHTML = 'Estas seguro que quieres eliminar la marca/modelo "' + data.marca + '"?';
            modal.find('.modal-body #marca_id').val(data.id);
        });
    });
}

var renderHtmlProd = function(modal, data){
    html = '<div class= "row">\
                <div class="center-block">\
                    <button type="button" id="eliminar" class="eliminar btn btn-danger btn-tabla center-block" data-toggle="modal" data-target=#delete-'+ modal +'-modal>\
                        <i class="fa fa-trash"></i>\
                    </button>\
                </div>\
                <div class="center-block">\
                    <a href="/admin/productos/'+ data +'/edit" class="editar btn btn-success btn-tabla center-block" style="margin-bottom: 0px;">\
                        <i class="fa fa-pencil"></i>\
                    </a>\
                </div>\
            </div>'
    return html;
}

var renderHtml = function(modal, data){
    html = '<div class= "row">\
                <div class="center-block">\
                    <button type="button" id="eliminar" class="eliminar btn btn-danger btn-tabla center-block" data-toggle="modal" data-target=#delete-'+ modal +'-modal>\
                        <i class="fa fa-trash"></i>\
                    </button>\
                </div>\
                <div class="center-block">\
                    <button type="button" id="editar" class="editar btn btn-success btn-tabla center-block" data-toggle="modal" data-target=#edit-'+ modal +'-modal style="margin-bottom: 0px;">\
                        <i class="fa fa-pencil"></i>\
                    </button>\
                </div>\
            </div>'
    return html;
}