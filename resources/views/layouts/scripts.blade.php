@section('scripts')
	<script> 
        var route = {
            'productos': '{{ route('get_data_productos') }}',
            'categorias': '{{ route('get_data_categorias') }}',
            'tipos': '{{ route('get_data_tipos') }}',
            'marcas': '{{ route('get_data_marcas') }}'
        };
    </script>
    <script src="{{asset ('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset ('js/vendor.js')}}"></script>
    <script src="{{asset ('js/app.js')}}"></script>
    <script src="{{asset ('js/modal-delete.js')}}"></script>
    <script src="{{asset ('js/tables.js')}}"></script>
    <script src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
@endsection