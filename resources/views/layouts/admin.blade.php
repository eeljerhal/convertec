<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }} - Administración</title>
        <link rel="shortcut icon" href="{{asset('img/logos/favicon.ico')}}">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
        @yield('styles')
        <link href="{{ asset('css/bootstrap-grid.css') }}" rel="stylesheet">
        <link href="{{ asset('css/vendor.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/mod-admin.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="main-wrapper">
            <div class="app" id="app">
                <header class="header">
                    <div class="header-block header-block-collapse hidden-lg-up"> <button class="collapse-btn" id="sidebar-collapse-btn">
                        <i class="fa fa-bars"></i>
                        </button> 
                    </div>
                    <!--<div class="header-block header-block-search hidden-sm-down">
                        <form role="search">
                            <div class="input-container">
                                <i class="fa fa-search"></i> <input type="search" placeholder="Buscar">
                                <div class="underline"></div>
                            </div>
                        </form>
                    </div>-->
                    <div class="header-block header-block-nav">
                        <ul class="nav-profile">
                            <li class="notifications new sidebar-li"></li>
                            <li class="profile dropdown sidebar-li">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                    <span class="name">
                                    {{ Auth::user()->name }}
                                    </span> 
                                </a>
                                <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <a class="dropdown-item" href="#">
                                    <i class="fa fa-user icon"></i>
                                    Perfil
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off icon"></i>
                                    Salir
                                    </a> 
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </header>
                <aside class="sidebar">
                    <div class="sidebar-container">
                        <div class="sidebar-header">
                            <div class="brand">
                                <div class="logo"> <img class="logo-sidebar" src="{{asset('img/logos/convertec_icon.png')}}"> </div>
                                {{ config('app.name') }}
                            </div>
                        </div>
                        <nav class="menu">
                            <ul class="nav metismenu" id="sidebar-menu">
                                <li class="active sidebar-li"> <a href="{{ route('login') }}">
                                    <i class="fa fa-home"></i> Principal
                                    </a> 
                                </li>
                                <li class="sidebar-li">
                                    <a href="{{ route('productos.create') }}">
                                    <i class="fa fa-microchip"></i> Agregar Productos
                                    </a>
                                </li>
                                <li class="sidebar-li">
                                    <a href="{{ url('admin/instructivo') }}" class="margin-left">
                                    <i class="fa fa-info padding-right"></i> Instrucciones
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </aside>
                    <div class="sidebar-overlay" id="sidebar-overlay"></div>
                    <article class="content responsive-tables-page">
                        @yield('mensajes')
                        @yield('content')
                    </article>
                <footer class="footer footer-static">
                    <div class="footer-block author">
                        <ul>
                            <li>Copyright © 2018 & Convertec, Derechos Reservados</li>
                        </ul>
                    </div>
                    <div class="footer-block author">
                        <ul>
                            <li> Plantilla creada por <a href="https://github.com/modularcode">ModularCode</a> </li>
                        </ul>
                    </div>
                </footer>
                @yield('modals')
            </div>
        </div>
        <!-- Reference block for JS -->
        <script> 
            var route = {
                'productos': '{{ route('get_data_productos') }}',
                'categorias': '{{ route('get_data_categorias') }}',
                'tipos': '{{ route('get_data_tipos') }}',
                'marcas': '{{ route('get_data_marcas') }}',
            };
        </script>
        <script src="{{asset ('js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{asset ('js/vendor.js')}}"></script>
        <script src="{{asset ('js/app.js')}}"></script>
        <script src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
        @yield('scripts')
    </body>
</html>