@extends('layouts.admin')

@section('content')
    <div class="title-block">
        <h1 class="title"> Productos </h1>
        <p class="title-description"> Lista de productos en el sistema </p>
    </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-bordered table-hover" id="productos-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Imágen</th>
                                    <th>Código</th>
                                    <th>Categoría</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Marca/Modelo</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 center-block">
                <div class="title-block title-margin">
                    <div class="row justify-content-between">
                        <div class="col-md-6">
                            <h1 class="title"> Categorías </h1>
                            <p class="title-description"> Lista de categorías </p>
                        </div>
                        <div class="col-md-6">
                            <button type="button" id="agregar" class="btn btn-primary pull-right" data-toggle="modal" data-target="#add-categoria-modal">
                                <i class="fa fa-plus"></i>
                                Agregar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 center-block">
                <div class="title-block title-margin">
                    <div class="row justify-content-between">
                        <div class="col-md-6">
                            <h1 class="title"> Tipos </h1>
                            <p class="title-description"> Lista de los tipos de producto</p>
                        </div>
                        <div class="col-md-6">
                            <button type="button" id="agregar" class="btn btn-primary pull-right" data-toggle="modal" data-target="#add-tipo-modal">
                                <i class="fa fa-plus"></i>
                                Agregar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 center-block">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-bordered table-hover" id="categorias-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Categoría</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-5 center-block">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-bordered table-hover" id="tipos-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Tipo</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5 center-block">
                <div class="title-block title-margin">
                    <div class="row justify-content-between">
                        <div class="col-md-6">
                            <h1 class="title"> Marcas/Modelos </h1>
                            <p class="title-description"> Lista de marcas/modelos </p>
                        </div>
                        <div class="col-md-6">
                            <button type="button" id="agregar" class="btn btn-primary pull-right" data-toggle="modal" data-target="#add-marca-modal">
                                <i class="fa fa-plus"></i>
                                Agregar
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-block">
                        <table class="table table-bordered table-hover" id="marcas-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Marca/Modelo</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    <script src="{{asset ('js/modal-delete.js')}}"></script>
    <script src="{{asset ('js/tables.js')}}"></script>
@endsection

@include('admin.modals')
