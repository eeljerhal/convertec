@section('modals')
<!-- MODAL PRODUCTO -->
    <div class="modal fade" id="delete-producto-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i>Advertencia</h4>
                </div>
                <form action="{{route('productos.destroy', 'eliminar')}}" method="post">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p id="producto"></p>
                        <input type="hidden" name="producto_id" id="prod_id" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Si</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- FIN MODAL PRODUCTO -->
<!-- MODAL CATEGORIAS -->
    <div class="modal fade" id="add-categoria-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i>Nueva Categoría</h4>
                </div>
                <form action="{{route('categorias.store')}}" method="post">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Categoría:</label>
                            <input type="text" class="form-control boxed" name="categoria">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-categoria-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i>Advertencia</h4>
                </div>
                <form action="{{route('categorias.destroy', 'eliminar')}}" method="post">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p id="categoria"></p>
                        <input type="hidden" name="categoria_id" id="cat_id" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Si</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-categoria-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i>Editar Categoría</h4>
                </div>
                <form action="{{route('categorias.update', 'editar')}}" method="post">
                    {{method_field('patch')}}
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="categoria">Categoría: </label>
                            <input type="text" class="form-control boxed" id="txt_cat" name="categoria">
                            <input type="hidden" name="categoria_id" id="cat_id" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- FIN MODAL CATEGORIAS -->

<!-- MODAL TIPO -->
    <div class="modal fade" id="add-tipo-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i>Nuevo Tipo</h4>
                </div>
                <form action="{{route('tipos.store')}}" method="post">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tipo:</label>
                            <input type="text" class="form-control boxed" name="tipo">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-tipo-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i>Advertencia</h4>
                </div>
                <form action="{{route('tipos.destroy', 'eliminar')}}" method="post">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p id="tipo"></p>
                        <input type="hidden" name="tipo_id" id="tipo_id" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="delete-btn">Si</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-tipo-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i>Editar Tipo</h4>
                </div>
                <form action="{{route('tipos.update', 'editar')}}" method="post">
                    {{method_field('patch')}}
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="tipo">Tipo: </label>
                            <input type="text" class="form-control boxed" id="txt_tipo" name="tipo">
                            <input type="hidden" name="tipo_id" id="tipo_id" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- FIN MODAL TIPO -->

<!-- MODAL MARCA -->
    <div class="modal fade" id="add-marca-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i>Nueva Marca/Modelo</h4>
                </div>
                <form action="{{route('marcas.store')}}" method="post">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Marca/Modelo:</label>
                            <input type="text" class="form-control boxed" name="marca">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-marca-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i>Advertencia</h4>
                </div>
                <form action="{{route('marcas.destroy', 'eliminar')}}" method="post">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p id="marca"></p>
                        <input type="hidden" name="marca_id" id="marca_id" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="delete-btn">Si</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-marca-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i>Editar Marca/Modelo</h4>
                </div>
                <form action="{{route('marcas.update', 'editar')}}" method="post">
                    {{method_field('patch')}}
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="categoria">Marca/Modelo: </label>
                            <input type="text" class="form-control boxed" id="txt_marca" name="marca">
                            <input type="hidden" name="marca_id" id="marca_id" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- FIN MODAL MARCA -->
@endsection

@section('mensajes')
    <?php $message=Session::get('message')?>
    @if($message != '')
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            @if($message == 'categoria_add')
                Categoría agregada con exito
            @endif
            @if($message == 'categoria_del')
                Categoría eliminada con exito
            @endif
            @if($message == 'categoria_edit')
                Categoría actualizada con exito
            @endif
            @if($message == 'tipo_add')
                Tipo agregado con exito
            @endif
            @if($message == 'tipo_del')
                Tipo eliminado con exito
            @endif
            @if($message == 'tipo_edit')
                Tipo actualizado con exito
            @endif
            @if($message == 'marca_add')
                Marca/Modelo agregada con exito
            @endif
            @if($message == 'marca_del')
                Marca/Modelo eliminada con exito
            @endif
            @if($message == 'producto_del')
                Producto eliminado con exito
            @endif
            @if($message == 'producto_edit')
                Producto modificado con exito
            @endif
            @if($message == 'marca_edit')
                Marca/Modelo actualizada con exito
            @endif
        </div>
    @endif
@endsection