@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('css/component.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
    <div class="title-block">
        <h3 class="title"> Agregue un Nuevo Producto <span class="sparkline bar" data-type="bar"></span> </h3>
    </div>
    <form id="add_product" name="producto" method="POST" enctype="multipart/form-data" action="{{ route('productos.store') }}">
        {{ csrf_field() }}
        <div class="card card-block">
            <div class="form-group row">
                <label class="col-sm-1 form-control-label text-xs-right">
                Categoría:
                </label>
                <div class="col-sm-3">
                    <select class="c-select form-control boxed" name="categoria">
                        <option selected>Seleccione Categoría</option>
                        @foreach($categorias as $cat)
                            <option value="{{$cat->id}}">{{$cat->categoria}}</option>
                        @endforeach
                    </select>
                </div>
                <label class="col-sm-1 form-control-label text-xs-right">
                Tipo:
                </label>
                <div class="col-sm-3">
                    <select class="c-select form-control boxed" name="tipo">
                        <option selected>Seleccione Tipo</option>
                        @foreach($tipos as $tipo)
                            <option value="{{$tipo->id}}">{{$tipo->tipo}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 form-control-label text-xs-right">
                Marca:
                </label>
                <div class="col-sm-3">
                    <select class="c-select form-control boxed" name="marca">
                        <option selected>Seleccione Marca</option>
                        @foreach($marcas as $mar)
                            <option value="{{$mar->id}}">{{$mar->marca}}</option>
                        @endforeach
                    </select>
                </div>
                <label class="col-sm-1 form-control-label text-xs-right">
                    Código:
                </label>
                <div class="col-sm-3"> <input type="text" class="form-control boxed" name="codigo" id="codigo" value=""> </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 form-control-label text-xs-right">
                Descripción:
                </label>
                <div class="col-sm-10">
                    <textarea id="mytextarea" name="descripcion"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 form-control-label text-xs-right">
                    Imágen:
                </label>
                <div class="col-sm-10">
                    <div class="box js">
                        <input type="file" name="imagen" id="file-7" class="inputfile inputfile-6" accept="image/*" onchange="readURL(this);"/>
                        <label for="file-7"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Seleccione Imágen&hellip;</strong></label>
                        <img id="preview" src="#" alt="" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12"> <button type="submit" class="btn btn-primary" id="btn_guardar">
                    Guardar
                    </button> 
                </div>
            </div>
        </div>
    </form>
@endsection

@section('mensajes')
    <?php $message=Session::get('message')?>
    @if($message != '')
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            @if($message == 'producto_add')
                Producto Ingresado con exito.
            @endif
        </div>
    @endif
@endsection

@section('scripts')
    <script src="{{asset ('vendor/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset ('js/jquery.custom-file-input.js')}}"></script>
    <script src="{{asset ('js/textEditor_Image_cfg.js')}}"></script>
@endsection