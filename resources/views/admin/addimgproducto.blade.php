@extends('layouts.admin')

@section('content')
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
    <div class="title-block">
        <h3 class="title"> Agregue una imágen al Producto {{$prod->codigo}}<span class="sparkline bar" data-type="bar"></span> </h3>
    </div>
    <div class="card card-block">
        <div class="form-group row">
            <label class="col-sm-1 form-control-label text-xs-right">
                Imagenes:
            </label>
            <div class="col-sm-10">
                <div class="card card-block filepicker" id="dz-area">
                    <form  enctype="multipart/form-data" class="dropzone" id="imgDropzone" name="producto" method="POST" action="{{ route('upload') }}">
                        {{ csrf_field() }}
                        <div class="dz-message-block">
                            <div class="dz-message needsclick"> Arrastre la imágen aquí o haga click para abrir el selector.</div>
                        </div>
                        <div class="dropzone-previews" id="preview"></div>
                         <div class="fallback">
                        <input type="hidden" name="id_prod" id="id_prod" value="{{$prod->id}}">
                    </form>
                </div>
            </div>
        </div>
        <div class="row pull-right">
            <div class="col-sm-12"> <button type="submit" class="btn btn-primary" id="btn_guardar">
                Guardar
                </button> 
            </div>
        </div>
    </div>
@endsection