<?php

use Illuminate\Database\Seeder;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos')->insert([
            [
                'tipo' => "Bobina Móvil",
            ],
            [
                'tipo' => "Contactor",
            ],
            [
                'tipo' => "Digital",
            ],
            [
                'tipo' => "DPDT",
            ],
            [
                'tipo' => "Montaje a Riel Din",
            ],
            [
                'tipo' => "Shunt",
            ]
        ]);
    }
}
