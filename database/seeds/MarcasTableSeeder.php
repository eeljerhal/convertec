<?php

use Illuminate\Database\Seeder;

class MarcasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marcas')->insert([
            [
                'marca' => "Clion/14F08-E",
            ],
            [
                'marca' => "Clion/HHC68A-2Z",
            ],
            [
                'marca' => "Clion/PTF08A",
            ],
            [
                'marca' => "Greegoo/FL-21",
            ],
            [
                'marca' => "Greegoo/GLC1-D8011",
            ],
            [
                'marca' => "Greegoo/GLP1-D4011",
            ],
            [
                'marca' => "Greegoo/GR-96",
            ],
            [
                'marca' => "Greegoo/LC1-D1210",
            ],
            [
                'marca' => "Greegoo/LC1-D4011",
            ],
            [
                'marca' => "Greegoo/SE-DP3",
            ],
            [
                'marca' => "Greegoo/SE96U-9K4",
            ],
            [
                'marca' => "Klemsan/C1D-SA",
            ],
            [
                'marca' => "Meishuo/MPJ2-S-248-C",
            ],
            [
                'marca' => "NCR/NRP13-C12DH",
            ]
        ]);
    }
}
