<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
        $this->call(MarcasTableSeeder::class);
        $this->call(TiposTableSeeder::class);
        $this->call(ProductosTableSeeder::class);
        $this->call(ImgProductosTableSeeder::class);
    }
}
